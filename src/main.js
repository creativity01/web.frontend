import Vue from 'vue'
import App from './App.vue'
import Lazyload from './Lazyload'
import VueYoutube from 'vue-youtube'

Vue.config.productionTip = false

Vue.use(VueYoutube)

// axios
import axios from "./axios.js"
Vue.prototype.$http = axios

Vue.prototype.$http.interceptors.request.use(config => {
  config.headers.Authorization = 'Bearer ' + localStorage.getItem("creativity-user-token");
  return config
});

Vue.prototype.$apiUrl = "http://creativity.loc/api/"

Vue.directive("lazyload", Lazyload)

// Vue Router
import router from './router'

// Vuex Store
import store from './store/store'

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
