import Vue from 'vue'
import Router from 'vue-router'
import store from './store/store'

Vue.use(Router)
// function LogOut(from, to, next) {
//   store.commit('auth/UNSET_TOKEN');
//   next('/login');
// }
const router = new Router({
    // mode: 'history',
    routes: [

        {
            path: '',
            component:() => import('./components/Main.vue'),
            children: [
              {
                path: '/',
                name: 'home',
                components:{
                    main: () => import('./views/Home.vue')
                }
              },
              {
                path: '/login',
                name: 'login',
                components: {
                    main:() => import('./views/auth/Login.vue')
                }
              },
            //   {
            //     path: '/page2',
            //     name: 'page-2',
            //     component: () => import('./views/Page2.vue')
            //   },
              {
                path: '/video/:videoId',
                name: 'videoPage', 
                components:{
                  main: () => import('./views/video/VideoPage.vue')
                }
              },
            //   {
            //     path: '/video',
            //     name: 'video', 
            //     component: () => import('./views/video/Videos.vue')
            //   },
            ],
        },
        // {
        //     path: '',
        //     component: () => import('@/layouts/full-page/FullPage.vue'),
        //     children: [
        // // =============================================================================
        // // PAGES
        // // =============================================================================
        //       {
        //         path: '/login',
        //         name: 'page-login',
        //         component: () => import('@/views/pages/Login.vue')
        //       },
        //       {
        //         path: '/error-404',
        //         name: 'page-error-404',
        //         component: () => import('@/views/pages/Error404.vue')
        //       },
        //     ]
        // },
        // Redirect to 404 page, if no match found
        {
            path: '*',
            redirect: '/pages/error-404'
        }
    ],
})

router.beforeEach((to, from, next) => {
  if (to.name !== 'login' && !store.getters['auth/getToken']) next({ name: 'login' })
  else next()
})

export default router
