import router from '../../router'

export default {
    LOGIN({commit}, payload) {
        const data = {
            email: payload.email,
            password: payload.password
        }
        payload.$http.post('login', data).then(response => {
            commit('SET_TOKEN', [response.data.meta.token, response.data.data]);

            router.go(-1);

        }).catch(() => {
            commit('UNSET_PROCESS');
            commit('SET_ERROR');
        })
    },

    REGISTER({commit}, payload){
        const data = {
            name:payload.name,
            password:payload.password,
            email:payload.email
        }

        payload.$http.post('register', data).then(response => {
            commit('SET_TOKEN', [response.data.meta.token, response.data.data]);
        })

    },

    GET_AUTH(state) {
        if (state.token) {
            state.isAuthenticated = true
        }
    },

    GET_PERM({commit}, $http) {

        commit('PERMS_LOADED', false);

        $http.then(response => {
            commit('SET_PERMISSIONS', response.data.permissions);
            commit('PERMS_LOADED', true);
        }).catch(() => {
            commit('PERMS_LOADED', false);
            commit('UNSET_PROCESS');
            commit('SET_ERROR');
            router.push('/login');
        });
    }
}
